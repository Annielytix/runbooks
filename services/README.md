# Service Catalog

More information about the service catalog can be found in the [Service Inventory Catalog page](https://about.gitlab.com/handbook/engineering/infrastructure/library/service-inventory-catalog/).

The `stage-group-mapping.jsonnet` file is generated from
[`stages.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/stages.yml)
in the handbook by running `scripts/update-stage-groups-feature-categories`.
